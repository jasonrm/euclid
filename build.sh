#!/bin/sh

KUBERNETES_VERSIONS="v1.7.16 v1.8.15 v1.9.11 v1.10.13 v1.11.8 v1.12.6 v1.13.4"

if [[ -z "$1" ]]; then
    echo "missing image name"
    exit
fi
IMAGE=$1

docker login -u gitlab-ci-token -p ${CI_JOB_TOKEN} ${CI_REGISTRY}
for VERSION in ${KUBERNETES_VERSIONS}; do
    docker build --pull --build-arg KUBERNETES_VERSION=${VERSION} --tag ${IMAGE}:${VERSION} .
    docker push ${IMAGE}:${VERSION}
done
