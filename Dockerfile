# Building custom fork of frep until features are merged upstream
FROM golang:alpine as frep
RUN apk add --update git \
    && mkdir -p /go/src /go/bin \
    && chmod -R 777 /go \
    && export GOPATH=/go \
    && export PATH=/go/bin:$PATH \
    && go get github.com/jasonrm/frep

# #### #### #### ####
FROM alpine:latest
ARG KUBERNETES_VERSION
COPY --from=frep /go/bin/frep /usr/local/bin/frep
RUN apk add --update ca-certificates git \
    && apk add --update --virtual deps curl \
    && curl -L https://storage.googleapis.com/kubernetes-release/release/${KUBERNETES_VERSION}/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl \
    && chmod 755 /usr/local/bin/kubectl \
    && apk del --purge deps \
    && rm -rf /var/cache/apk/*
WORKDIR /root
CMD ["/bin/sh"]
